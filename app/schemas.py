from datetime import datetime
from typing import Optional
from pydantic import BaseModel, EmailStr, conint

class UserCreate(BaseModel):
    email: EmailStr
    password: str

class UserResponse(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime

    class Config:
        from_attributes = True

class UserLogin(BaseModel):
    email: EmailStr
    password: str

class PostBase(BaseModel):
    title: str
    content: str
    published: bool = True

class PostCreate(PostBase):
    pass

class PostResponse(PostBase):
    id: int
    created_at: datetime
    owner_id: int
    owner: UserResponse
    
    class Config:
        from_attributes = True # because by default pydantic model reads data if it is a dictionary but we are returning sqlalchemy model

class PostVote(BaseModel):
    Post: PostResponse
    votes: int
    
    class Config:
        from_attributes = True # because by default pydantic model reads data if it is a dictionary but we are returning sqlalchemy model

class TokenResponse(BaseModel):
    Token: str
    Token_type: str

class TokenData(BaseModel):
    user_id: Optional[int] = None

class Vote(BaseModel):
    post_id: int
    dir: bool