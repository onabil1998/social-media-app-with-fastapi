from fastapi import APIRouter, status, HTTPException, Response, Depends
from sqlalchemy.orm import Session
from .. import schemas, oauth2, models
from ..database import get_db

router = APIRouter(
    prefix="/vote"
)

@router.post("/", status_code=status.HTTP_201_CREATED)
def vote(vote: schemas.Vote, db: Session = Depends(get_db), current_user: int = Depends(oauth2.get_current_user)):
    post_exist = db.query(models.Post).filter(models.Post.id == vote.post_id).first()
    if not post_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"post with id {vote.post_id} is not found")

    vote_query = db.query(models.Vote).filter(models.Vote.post_id == vote.post_id, models.Vote.user_id == current_user.id)
    found_vote = vote_query.first()

    if vote.dir == 1:
        if found_vote != None:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"You already voted the post {vote.post_id} before")
        
        vote_entry = models.Vote(post_id = vote.post_id, user_id = current_user.id)
        db.add(vote_entry)
        db.commit()

        return {"message": "Successfully voted the post"}
    
    else:
        if found_vote == None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="the post is not voted already")
        
        vote_query.delete(synchronize_session=False)
        db.commit()

        return {"message": "Successfully unvoted the post"}