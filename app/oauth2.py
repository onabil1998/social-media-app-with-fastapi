from fastapi import Depends
from jose import jwt, JWTError
from datetime import datetime, timedelta
from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordBearer
from . import schemas, models
from .database import get_db
from .config import settings

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

SECRET_KEY = settings.SECRET_KEY
ALGORITHM = settings.ALGORITHM
ACCESS_TOKEN_EXPIRE_MINUTES = settings.ACCESS_TOKEN_EXPIRE_MINUTES

def create_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    jwt_token = jwt.encode(to_encode, SECRET_KEY, ALGORITHM)

    return jwt_token

def verify_token(token: str, exception):

    try:

        decoded_token = jwt.decode(token, SECRET_KEY, [ALGORITHM])
        user_id: int = decoded_token.get("user_id")

        if user_id == None:
            raise exception
        token_data = schemas.TokenData(user_id=user_id)

    except JWTError:
        raise exception
    
    return token_data

def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="can't validate credentials", 
                              headers={"WWW-Authenticate": "Bearer"})

    token_data = verify_token(token, exception)
    user = db.query(models.User).filter(models.User.id == token_data.user_id).first()

    return user