"""Add columns to posts table

Revision ID: 66e998693a67
Revises: ee4a5ed9c98a
Create Date: 2023-09-12 23:29:33.665574

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '66e998693a67'
down_revision: Union[str, None] = 'ee4a5ed9c98a'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column("posts", sa.Column("content", sa.String(), nullable = False))
    op.add_column("posts", sa.Column("published", sa.Boolean(), nullable = False, server_default='TRUE')) 
    op.add_column("posts", sa.Column("created_at", sa.TIMESTAMP(timezone=True), nullable = False, server_default = sa.text("now()")))
    pass


def downgrade() -> None:
    op.drop_column("posts", "content")
    op.drop_column("posts", "published")
    op.drop_column("posts", "created_at")
    pass
